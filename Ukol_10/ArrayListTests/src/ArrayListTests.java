import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayListTests {
    @Test
    public void testIndexOfExistingElement() {
        // Vytvoření ArrayListu
        ArrayList<String> list = new ArrayList<>();
        list.add("Jablko");
        list.add("Hruška");
        list.add("Banán");

        // Testování indexu existujícího prvku
        assertEquals(1, list.indexOf("Hruška"));
    }

    @Test
    public void testIndexOfNonExistingElement() {
        // Vytvoření ArrayListu
        ArrayList<String> list = new ArrayList<>();
        list.add("Jablko");
        list.add("Hruška");
        list.add("Banán");

        // Testování indexu neexistujícího prvku
        assertEquals(-1, list.indexOf("Kiwi"));
    }

    @Test
    public void testIndexOfNullElement() {
        // Vytvoření ArrayListu
        ArrayList<String> list = new ArrayList<>();
        list.add("Jablko");
        list.add(null);
        list.add("Banán");

        // Testování indexu null prvku
        assertEquals(1, list.indexOf(null));
    }

    @Test
    public void testIndexOfEmptyList() {
        // Vytvoření prázdného ArrayListu
        ArrayList<String> list = new ArrayList<>();

        // Testování indexu prvku v prázdném seznamu
        assertEquals(-1, list.indexOf("Jablko"));
    }

    @Test
    public void testIndexOfDuplicateElements() {
        // Vytvoření ArrayListu s duplicitními prvky
        ArrayList<String> list = new ArrayList<>();
        list.add("Jablko");
        list.add("Hruška");
        list.add("Jablko");

        // Testování indexu duplicitního prvku
        assertEquals(0, list.indexOf("Jablko"));
    }
}
