public class Fibonacci {
    private int[] fibTable;
    private int computationCounter;
    public int assignmentOperations;
    public int comparisonOperations;
    public int arithmeticOperations;

    public Fibonacci() {
        computationCounter = 0; // Inicializace čítače
        assignmentOperations = 0;
        comparisonOperations = 0;
        arithmeticOperations = 0;
    }

    public void initCalcSlozitost() {
        computationCounter = 0;
        assignmentOperations = 0;
        comparisonOperations = 0;
        arithmeticOperations = 0;
    }

    public int getComputationCounter() {
        return computationCounter;
    }

    public int getAssignmentOperations() {
        return assignmentOperations;
    }

    public int getComparisonOperations() {
        return comparisonOperations;
    }

    public int getArithmeticOperations() {
        return arithmeticOperations;
    }

    // Ostatní metody zůstávají nezměněné

    public void init(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Vstup musí být nezáporný.");
        }

        fibTable = new int[n + 1];
        fibTable[0] = 0;
        fibTable[1] = 1;

        for (int i = 2; i <= n; i++) {
            fibTable[i] = -1; // Počáteční hodnota -1 indikuje, že hodnota ještě nebyla spočítána
        }
    }

    public int calcNerek(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Vstup musí být nezáporný.");
        }

        computationCounter++; // Zvýšení čítače při každém výpočtu

        if (n <= 1)
            return n;

        int prev = 0;
        int current = 1;

        for (int i = 2; i <= n; i++) {
            int next = prev + current;
            prev = current;
            current = next;

            assignmentOperations += 2; // Při každé iteraci proběhnou 2 přiřazení
            arithmeticOperations++; // Při každé iteraci proběhne aritmetická operace (sčítání)
        }

        return current;
    }

    public int calcRek(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Vstup musí být nezáporný.");
        }

        computationCounter++; // Zvýšení čítače při každém výpočtu

        if (n <= 1)
            return n;
        else {
            comparisonOperations++; // Porovnání pro podmínku n <= 1
            return calcRek(n - 1) + calcRek(n - 2);
        }
    }

    public int calcRekTable(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Vstup musí být nezáporný.");
        }

        computationCounter++; // Zvýšení čítače při každém výpočtu

        if (fibTable == null || fibTable.length <= n) {
            throw new IllegalArgumentException("Tabulka pro Fibonacciho posloupnost není inicializována nebo má nedostatečnou velikost.");
        }

        if (fibTable[n] == -1) {
            fibTable[n] = calcRekTable(n - 1) + calcRekTable(n - 2);
            // Výpis stavu tabulky
            System.out.println("Stav tabulky po výpočtu pro index " + n + ":");
            for (int i = 0; i <= n; i++) {
                System.out.print(fibTable[i] + " ");
            }
            System.out.println();
            arithmeticOperations++; // Při každém výpočtu hodnoty do tabulky proběhne aritmetická operace (sčítání)
        }

        return fibTable[n];
    }
}
