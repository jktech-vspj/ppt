import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest {
    private Fibonacci fibonacci;

    @BeforeEach
    public void setUp() {
        fibonacci = new Fibonacci();
    }

    @Test
    public void testCalcNerek_GeneralCases() {
        assertEquals(1, fibonacci.calcNerek(2));
        assertEquals(2, fibonacci.calcNerek(3));
        assertEquals(3, fibonacci.calcNerek(4));
        assertEquals(5, fibonacci.calcNerek(5));
        assertEquals(8, fibonacci.calcNerek(6));
        assertEquals(13, fibonacci.calcNerek(7));
    }

    @Test
    public void testCalcNerek_EdgeCases() {
        assertEquals(0, fibonacci.calcNerek(0));
        assertEquals(1, fibonacci.calcNerek(1));
        assertThrows(IllegalArgumentException.class, () -> fibonacci.calcNerek(-1)); // Negative input
    }

    @Test
    public void testCalcRek_GeneralCases() {
        assertEquals(1, fibonacci.calcRek(2));
        assertEquals(2, fibonacci.calcRek(3));
        assertEquals(3, fibonacci.calcRek(4));
        assertEquals(5, fibonacci.calcRek(5));
        assertEquals(8, fibonacci.calcRek(6));
        assertEquals(13, fibonacci.calcRek(7));
    }

    @Test
    public void testCalcRek_EdgeCases() {
        assertEquals(0, fibonacci.calcRek(0));
        assertEquals(1, fibonacci.calcRek(1));
        assertThrows(IllegalArgumentException.class, () -> fibonacci.calcRek(-1));
    }

    @Test
    public void testCalcRekTable_GeneralCases() {
        fibonacci.init(7);

        assertEquals(1, fibonacci.calcRekTable(2));
        assertEquals(2, fibonacci.calcRekTable(3));
        assertEquals(3, fibonacci.calcRekTable(4));
        assertEquals(5, fibonacci.calcRekTable(5));
        assertEquals(8, fibonacci.calcRekTable(6));
        assertEquals(13, fibonacci.calcRekTable(7));
    }

    @Test
    public void testCalcRekTable_EdgeCases() {
        fibonacci.init(2);

        assertEquals(0, fibonacci.calcRekTable(0));
        assertEquals(1, fibonacci.calcRekTable(1));
        assertEquals(1, fibonacci.calcRekTable(2));
        assertThrows(IllegalArgumentException.class, () -> fibonacci.calcRekTable(-1)); // Negative input
    }
}