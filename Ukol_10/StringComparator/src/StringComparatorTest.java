import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringComparatorTest {
    @Test
    public void testCompareStrings() {
        String str1 = "ABCDEF";
        String str2 = "ABCD";
        int result = StringComparator.compareStrings(str1, str2);
        assertEquals(2, result);
    }
}