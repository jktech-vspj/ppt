#pragma once
#include <iostream>

class Obdelnik_IO {
public:
    // Funkce pro na�ten� d�lky a ���ky obd�ln�ku
    static void nactiObdelnik(double& delka, double& sirka);

    // Funkce pro v�pis informac� o obd�ln�ku
    static void vypisObdelnik(const double delka, const double sirka);
};
