﻿#include "Obdelnik_IO.h"

void Obdelnik_IO::nactiObdelnik(double& delka, double& sirka) {
    std::cout << "Zadejte delku obdelniku: ";
    std::cin >> delka;
    std::cout << "Zadejte sirku obdelniku: ";
    std::cin >> sirka;
}

void Obdelnik_IO::vypisObdelnik(const double delka, const double sirka) {
    double obvod = 2 * (delka + sirka);
    double obsah = delka * sirka;

    std::cout << "Obdelnik ma delku: " << delka << " a sirku: " << sirka << std::endl;
    std::cout << "Obvod obdelniku je: " << obvod << std::endl;
    std::cout << "Obsah obdelniku je: " << obsah << std::endl;
}
