#include "Obdelnik_IO.h"

int main() {
    double delka, sirka;

    // Na�ten� obd�ln�ku
    Obdelnik_IO::nactiObdelnik(delka, sirka);

    // V�pis informac� o obd�ln�ku
    Obdelnik_IO::vypisObdelnik(delka, sirka);

    return 0;
}
