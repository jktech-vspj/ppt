﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public static class CustomMath
    {
        public static int Factorial(int number)
        {
            if (number < 0)
                throw new ArgumentException("Číslo musí být větší jak 0");
            int result = 1;
            for (int i = 2; i <= number; i++)
                result *= i;
            return result;
        }
    }

