﻿using System;
using static CustomMath;

class Program
{
    static void Main(string[] args)
    {
        int number = 5;
        Console.WriteLine($"Faktoriál čísla {number} je {CustomMath.Factorial(number)}");
        Console.WriteLine($"Druhá odmocnina čísla {number} je {Math.Sqrt(number)}");
    }
}
