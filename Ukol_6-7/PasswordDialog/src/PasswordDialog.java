import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PasswordDialog extends JFrame {
    private final JPasswordField passwordField;
    private final JCheckBox showPasswordCheckBox;
    private final JLabel passwordStrengthLabel;

    public PasswordDialog() {
        setTitle("Heslo");
        setSize(300, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(3, 1));

        // Password field
        passwordField = new JPasswordField();
        passwordField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPasswordStrength();
            }
        });
        add(passwordField);

        // Show password checkbox
        showPasswordCheckBox = new JCheckBox("Zobrazit heslo");
        showPasswordCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                togglePasswordVisibility();
            }
        });
        add(showPasswordCheckBox);

        // Password strength label
        passwordStrengthLabel = new JLabel();
        add(passwordStrengthLabel);

        setVisible(true);
    }

    public void togglePasswordVisibility() {
        if (showPasswordCheckBox.isSelected()) {
            passwordField.setEchoChar((char) 0);
        }

        else {
            passwordField.setEchoChar('*');
        }
    }

    public void checkPasswordStrength() {
        String password = new String(passwordField.getPassword());
        int strength = calculatePasswordStrength(password);
        String strengthText;
        if (strength < 30) {
            strengthText = "Slabé";
        }

        else if (strength < 60) {
            strengthText = "Střední";
        }

        else {
            strengthText = "Silné";
        }
        passwordStrengthLabel.setText("Síla hesla: " + strengthText);
    }

    public String getPasswordStrengthLabel() {
        return passwordStrengthLabel.getText();
    }


    public int calculatePasswordStrength(String password) {
        if (password == null || password.isEmpty()) {
            return 0; // Weak password
        }

        int strength = 0;
        int length = password.length();
        boolean hasUpperCase = false;
        boolean hasLowerCase = false;
        boolean hasNumber = false;
        boolean hasSpecialChar = false;

        for (int i = 0; i < length; i++) {
            char ch = password.charAt(i);
            if (Character.isUpperCase(ch)) {
                hasUpperCase = true;
            }

            else if (Character.isLowerCase(ch)) {
                hasLowerCase = true;
            }

            else if (Character.isDigit(ch)) {
                hasNumber = true;
            }

            else {
                hasSpecialChar = true;
            }
        }

        // Bonus points for different character types
        if (hasUpperCase) strength += 10;
        if (hasLowerCase) strength += 10;
        if (hasNumber) strength += 10;
        if (hasSpecialChar) strength += 10;

        // Bonus points for length
        strength += length * 2;

        return strength;
    }

    public void setPasswordField(String password) {
        passwordField.setText(password);
    }

    public boolean isPasswordVisible() {
        return (passwordField.getEchoChar() == '*');
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new PasswordDialog();
            }
        });
    }
}
