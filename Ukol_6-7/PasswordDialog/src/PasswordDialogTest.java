import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PasswordDialogTest {

    @Test
    public void testPasswordFieldVisibility() {
        PasswordDialog passwordDialog = new PasswordDialog();
        assertFalse(passwordDialog.isPasswordVisible());
        passwordDialog.togglePasswordVisibility();
        assertTrue(passwordDialog.isPasswordVisible());
    }

    @Test
    public void testCheckPasswordStrength() {
        PasswordDialog passwordDialog = new PasswordDialog();

        passwordDialog.setPasswordField("weak");
        passwordDialog.checkPasswordStrength();
        assertEquals("Síla hesla: Slabé", passwordDialog.getPasswordStrengthLabel());

        passwordDialog.setPasswordField("Medium1");
        passwordDialog.checkPasswordStrength();
        assertEquals("Síla hesla: Střední", passwordDialog.getPasswordStrengthLabel());

        passwordDialog.setPasswordField("Strong@Password123");
        passwordDialog.checkPasswordStrength();
        assertEquals("Síla hesla: Silné", passwordDialog.getPasswordStrengthLabel());
    }
}
