import static org.junit.Assert.*;
import org.junit.Test;

public class TrigonometryTest {

    @Test
    public void testMathSin() {
        // Testujeme, zda Math.sin() vrací správné hodnoty pro různé úhly včetně extrémů
        assertEquals(0.0, Math.sin(Math.toRadians(0)), 0.001); // sin(0) = 0
        assertEquals(1.0, Math.sin(Math.toRadians(90)), 0.001); // sin(90) = 1
        assertEquals(0.707, Math.sin(Math.toRadians(45)), 0.001); // sin(45) = sqrt(2)/2
        assertEquals(-1.0, Math.sin(Math.toRadians(270)), 0.001); // sin(270) = -1
    }

    @Test
    public void testMathTan() {
        // Testujeme, zda Math.tan() vrací správné hodnoty pro různé úhly včetně extrémů
        assertEquals(0.0, Math.tan(Math.toRadians(0)), 0.001); // tg(0) = 0
        assertEquals(1.0, Math.tan(Math.toRadians(45)), 0.001); // tg(45) = 1

        // Test na úhly, pro které není definovaná tg funkce
        assertTrue(Double.isNaN(Math.tan(Math.toRadians(90)))); // tg(90) není definováno
        assertTrue(Double.isNaN(Math.tan(Math.toRadians(270)))); // tg(270) není definováno
    }
}
