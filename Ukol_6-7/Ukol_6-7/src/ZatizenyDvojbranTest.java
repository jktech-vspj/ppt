import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ZatizenyDvojbranTest {

    @Test
    public void testVypocet() {
        ZatizenyDvojbran dvojbran = new ZatizenyDvojbran(10.5, 20.0, 5.0);
        assertEquals(6.45, dvojbran.getVystupniNapeti(), 0.01);
        assertEquals(1.29, dvojbran.getVystupniProud(), 0.01);
    }

    @Test
    public void testNegativeResistance() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(-10.5, 20.0, 5.0);
        });
    }

    @Test
    public void testNegativeVoltage() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(10.5, -20.0, 5.0);
        });
    }

    @Test
    public void testNegativeImpedance() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(10.5, 20.0, -5.0);
        });
    }

    @Test
    public void testZeroResistance() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(0, 20.0, 5.0);
        });
    }

    @Test
    public void testZeroVoltage() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(10.5, 0, 5.0);
        });
    }

    @Test
    public void testZeroImpedance() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ZatizenyDvojbran(10.5, 20.0, 0);
        });
    }
}
