import java.text.DecimalFormat;

public class ZatizenyDvojbran {
    private double rezistance;
    private double napeti;
    private double impedance;
    private double vystupniNapeti;
    private double vystupniProud;

    public ZatizenyDvojbran(double rezistance, double napeti, double impedance) {
        if (rezistance <= 0 || napeti <= 0 || impedance <= 0) {
            throw new IllegalArgumentException("Hodnoty rezistance, napeti a impedance musí být větší než 0.");
        }

        this.rezistance = rezistance;
        this.napeti = napeti;
        this.impedance = impedance;
        vypocet();
    }

    private void vypocet() {
        vystupniNapeti = (napeti * impedance) / (rezistance + impedance);
        vystupniProud = vystupniNapeti / impedance;

        // Zaokrouhlení na dvě desetinná místa
        DecimalFormat df = new DecimalFormat("#.##");
        vystupniNapeti = Double.parseDouble(df.format(vystupniNapeti));
        vystupniProud = Double.parseDouble(df.format(vystupniProud));

        // Vypis výsledku
        vypisVysledek();
    }

    private void vypisVysledek() {
        System.out.println("Výstupní napětí: " + vystupniNapeti);
        System.out.println("Výstupní proud: " + vystupniProud);
    }

    public double getVystupniNapeti() {
        return vystupniNapeti;
    }

    public double getVystupniProud() {
        return vystupniProud;
    }
}
