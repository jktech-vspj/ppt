import org.junit.Test;
import static org.junit.Assert.*;

import java.util.HashMap;

public class DictionaryTest {

    @Test
    public void testAdd() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("apple", "jablko");
        assertEquals("jablko", dictionary.get("apple"));
    }

    @Test
    public void testUpdateValue() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("apple", "jablko");
        dictionary.put("apple", "hruška"); // aktualizuje hodnotu pro existující klíč
        assertEquals("hruška", dictionary.get("apple"));
    }

    @Test
    public void testNoUpdateForExistingKey() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("apple", "jablko");
        dictionary.put("apple", "hruška"); // aktualizuje hodnotu pro existující klíč
        dictionary.putIfAbsent("apple", "banán"); // nepovoluje aktualizaci, mělo by zůstat "hruška"
        assertEquals("hruška", dictionary.get("apple"));
    }

    @Test
    public void testEmptyKeyOrValue() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("", "emptyKey");
        dictionary.put("nonEmptyKey", "");
        assertEquals("emptyKey", dictionary.get(""));
        assertEquals("", dictionary.get("nonEmptyKey"));
    }

    @Test
    public void testAddNullValue() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("nullKey", null);
        assertNull(dictionary.get("nullKey"));
    }

    @Test
    public void testAddNullKey() {
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put(null, "nullValue");
        assertEquals("nullValue", dictionary.get(null));
    }
}
