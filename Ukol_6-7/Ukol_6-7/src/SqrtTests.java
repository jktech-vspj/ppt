import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SqrtTests {

    @Test
    public void testSquareRootPositive() {
        // Testování odmocniny kladné hodnoty
        double positiveNumber = 25.0;
        double computedResult = Math.sqrt(positiveNumber);
        assertEquals(computedResult, 5.0, 0.0001);
    }

    @Test
    public void testSquareRootZero() {
        // Testování odmocniny nulové hodnoty
        double zeroNumber = 0.0;
        double computedResult = Math.sqrt(zeroNumber);
        assertEquals(computedResult, 0.0, 0.0001);
    }

    @Test
    public void testSquareRootNegative() {
        // Testování odmocniny záporné hodnoty
        double negativeNumber = -25.0;
        double computedResult = Math.sqrt(negativeNumber);
        assertTrue(Double.isNaN(computedResult));
    }

    @Test
    public void testSquareRootLargePositive() {
        // Testování odmocniny velké kladné hodnoty
        double largePositiveNumber = 1.0e20;
        double computedResult = Math.sqrt(largePositiveNumber);
        double expected = 1.0e10;
        assertEquals(computedResult, expected, 0.0001);
    }
}
