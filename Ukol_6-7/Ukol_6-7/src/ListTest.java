import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;

public class ListTest {

    // Test pro prázdný seznam
    @Test
    public void testIndexOfEmptyList() {
        List<String> list = new java.util.ArrayList<>();
        assertEquals(-1, list.indexOf("test"));
    }

    // Test pro seznam s jedním prvkem, který se shoduje s hledaným prvkem
    @Test
    public void testIndexOfSingleElementList() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        assertEquals(0, list.indexOf("test"));
    }

    // Test pro seznam s jedním prvkem, který se neshoduje s hledaným prvkem
    @Test
    public void testIndexOfSingleElementListNotFound() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        assertEquals(-1, list.indexOf("abc"));
    }

    // Test pro seznam s více prvky, kde je hledaný prvek na začátku seznamu
    @Test
    public void testIndexOfElementAtBeginning() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        list.add("example");
        list.add("data");
        assertEquals(0, list.indexOf("test"));
    }

    // Test pro seznam s více prvky, kde je hledaný prvek uprostřed seznamu
    @Test
    public void testIndexOfElementInMiddle() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        list.add("example");
        list.add("data");
        assertEquals(1, list.indexOf("example"));
    }

    // Test pro seznam s více prvky, kde je hledaný prvek na konci seznamu
    @Test
    public void testIndexOfElementAtEnd() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        list.add("example");
        list.add("data");
        assertEquals(2, list.indexOf("data"));
    }

    // Test pro hledání prvku, který se v seznamu vyskytuje vícekrát
    @Test
    public void testIndexOfDuplicateElement() {
        List<String> list = new java.util.ArrayList<>();
        list.add("test");
        list.add("example");
        list.add("test");
        assertEquals(0, list.indexOf("test"));
    }

    // Test pro hledání prvku v seznamu, který obsahuje null hodnotu
    @Test
    public void testIndexOfNullElement() {
        List<String> list = new java.util.ArrayList<>();
        list.add(null);
        assertEquals(0, list.indexOf(null));
    }
}
