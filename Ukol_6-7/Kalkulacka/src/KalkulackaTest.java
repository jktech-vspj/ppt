import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class KalkulackaTest {

    @Test
    public void testScitani() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("5");
        kalkulacka.getCislo2Field().setText("3");
        kalkulacka.getOperaceComboBox().setSelectedItem("+");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("8.0", kalkulacka.getVysledekField().getText());
    }

    @Test
    public void testOdcitani() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("8");
        kalkulacka.getCislo2Field().setText("3");
        kalkulacka.getOperaceComboBox().setSelectedItem("-");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("5.0", kalkulacka.getVysledekField().getText());
    }

    @Test
    public void testNasobeni() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("4");
        kalkulacka.getCislo2Field().setText("5");
        kalkulacka.getOperaceComboBox().setSelectedItem("*");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("20.0", kalkulacka.getVysledekField().getText());
    }

    @Test
    public void testDeleni() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("10");
        kalkulacka.getCislo2Field().setText("2");
        kalkulacka.getOperaceComboBox().setSelectedItem("/");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("5.0", kalkulacka.getVysledekField().getText());
    }

    @Test
    public void testDeleniNulou() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("5");
        kalkulacka.getCislo2Field().setText("0");
        kalkulacka.getOperaceComboBox().setSelectedItem("/");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("", kalkulacka.getVysledekField().getText());
    }

    @Test
    public void testNeplatneVstupy() {
        Kalkulacka kalkulacka = new Kalkulacka();
        kalkulacka.getCislo1Field().setText("a");
        kalkulacka.getCislo2Field().setText("b");
        kalkulacka.getOperaceComboBox().setSelectedItem("+");
        kalkulacka.getSpocitejButton().doClick();
        assertEquals("", kalkulacka.getVysledekField().getText());
    }
}
