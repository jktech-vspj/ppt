import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Kalkulacka extends JFrame implements ActionListener {
    private final JComboBox<String> operaceComboBox;
    private final JTextField cislo1Field, cislo2Field, vysledekField;
    private final JButton spocitejButton;

    public Kalkulacka() {
        setTitle("Kalkulačka");
        setSize(300, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(2, 4));

        cislo1Field = new JTextField();
        add(cislo1Field);

        operaceComboBox = new JComboBox<>(new String[]{"+", "-", "*", "/"});
        add(operaceComboBox);

        cislo2Field = new JTextField();
        add(cislo2Field);

        spocitejButton = new JButton("Spočítej");
        spocitejButton.addActionListener(this);
        add(spocitejButton);

        vysledekField = new JTextField();
        vysledekField.setEditable(false);
        add(vysledekField);

        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String operace;
        double cislo1, cislo2, vysledek;

        vysledekField.setText("");

        if (e.getSource() == spocitejButton) {
            try {
                operace = (String) operaceComboBox.getSelectedItem();
                cislo1 = Double.parseDouble(cislo1Field.getText());
                cislo2 = Double.parseDouble(cislo2Field.getText());
                vysledek = 0;
            }

            catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Zadávejte pouze číselné hodnoty.");
                return;
            }

            switch (operace) {
                case "+":
                    vysledek = cislo1 + cislo2;
                    break;
                case "-":
                    vysledek = cislo1 - cislo2;
                    break;
                case "*":
                    vysledek = cislo1 * cislo2;
                    break;
                case "/":
                    if (cislo2 != 0)
                        vysledek = cislo1 / cislo2;
                    else {
                        JOptionPane.showMessageDialog(this, "Dělení nulou není možné.");
                        return;
                    }

                    break;
                default:
                    JOptionPane.showMessageDialog(this, "Dělení nulou není možné.");
                break;

            }

            vysledekField.setText(Double.toString(vysledek));
        }
    }

    public JTextField getCislo1Field() {
        return cislo1Field;
    }

    public JTextField getCislo2Field() {
        return cislo2Field;
    }

    public JComboBox<String> getOperaceComboBox() {
        return operaceComboBox;
    }

    public JButton getSpocitejButton() {
        return spocitejButton;
    }

    public JTextField getVysledekField() {
        return vysledekField;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Kalkulacka();
            }
        });
    }
}
