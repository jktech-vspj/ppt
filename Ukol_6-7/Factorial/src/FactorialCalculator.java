import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;

public class FactorialCalculator extends JFrame implements ActionListener {
    public final JTextField inputField;
    public final JButton calculateButton;
    public final JLabel resultLabel;

    public FactorialCalculator() {
        setTitle("Kalkulátor Faktoriálu");
        setSize(300, 150);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 1));

        JLabel inputLabel = new JLabel("Zadejte číslo:");
        inputField = new JTextField();
        inputField.addActionListener(this);

        calculateButton = new JButton("Spočítat");
        calculateButton.addActionListener(this);

        resultLabel = new JLabel();

        panel.add(inputLabel);
        panel.add(inputField);
        panel.add(calculateButton);
        panel.add(resultLabel);

        add(panel);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == calculateButton || e.getSource() == inputField) {
            String inputText = inputField.getText();
            try {
                int number = Integer.parseInt(inputText);
                if (number < 0) {
                    showError("Negativní čísla nejsou povolena.");
                } else {
                    BigInteger factorial = MyMath.factorial(number);
                    resultLabel.setText("Faktoriál: " + factorial.toString());
                }
            } catch (NumberFormatException ex) {
                showError("Chyba: Zadaná hodnota není platné celé číslo.");
            }
        }
    }

    private void showError(String message) {
        JOptionPane.showMessageDialog(this, message, "Chyba", JOptionPane.ERROR_MESSAGE);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            FactorialCalculator calculator = new FactorialCalculator();
            calculator.setVisible(true);
        });
    }
}