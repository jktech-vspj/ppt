import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import static org.junit.jupiter.api.Assertions.*;

public class FactorialCalculatorTest {
    private FactorialCalculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new FactorialCalculator();
    }

    @Test
    public void testFactorialCalculatorNotNull() {
        assertNotNull(calculator);
    }

    @Test
    public void testInputFieldNotNull() {
        assertNotNull(calculator.inputField);
    }

    @Test
    public void testCalculateButtonNotNull() {
        assertNotNull(calculator.calculateButton);
    }

    @Test
    public void testResultLabelNotNull() {
        assertNotNull(calculator.resultLabel);
    }

    @Test
    public void testActionPerformedWithNegativeInput() {
        ActionEvent event = new ActionEvent(calculator.inputField, ActionEvent.ACTION_PERFORMED, "");
        calculator.inputField.setText("-5");
        calculator.actionPerformed(event);
        assertEquals("", calculator.resultLabel.getText());
    }

    @Test
    public void testActionPerformedWithNonIntegerInput() {
        ActionEvent event = new ActionEvent(calculator.inputField, ActionEvent.ACTION_PERFORMED, "");
        calculator.inputField.setText("abc");
        calculator.actionPerformed(event);
        assertEquals("", calculator.resultLabel.getText());
    }

    @Test
    public void testActionPerformedWithValidInput() {
        ActionEvent event = new ActionEvent(calculator.inputField, ActionEvent.ACTION_PERFORMED, "");
        calculator.inputField.setText("5");
        calculator.actionPerformed(event);
        assertEquals("Faktoriál: 120", calculator.resultLabel.getText());
    }

    @Test
    public void testMainMethod() {
        SwingUtilities.invokeLater(() -> {
            FactorialCalculator.main(new String[]{});
            assertTrue(calculator.isVisible());
            calculator.dispatchEvent(new WindowEvent(calculator, WindowEvent.WINDOW_CLOSING));
        });
    }
}