import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Map;

public class CsvExporter implements Exporter {
    @Override
    public void exportData(Map<String, Trida> tridy) {
        for (Map.Entry<String, Trida> entry : tridy.entrySet())
        {
            String tridaNazev = entry.getKey();
            Trida trida = entry.getValue();
            String souborNazev = tridaNazev + ".csv";

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(souborNazev))) {
                writer.write("Trida: " + tridaNazev + "\n");
                for (Student student : trida.getStudenti()) {
                    if (student != null) {
                        writer.write(student.getJmeno() + " " + student.getPrijmeni() + ": " + student.vypoctiPrumerZnamek() + "\n");
                    }
                }
                writer.write("Průměr třídy " + tridaNazev + " je " + trida.vypoctiTridniPrumer() + "\n\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
