public class ExporterFactory {
    public static Exporter getExporter(String type) {
        switch (type.toLowerCase()) {
            case "csv":
                return new CsvExporter();
            case "xml":
                return new XmlExporter();
            default:
                throw new IllegalArgumentException("Unsupported exporter type: " + type);
        }
    }
}
