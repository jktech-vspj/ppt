import java.util.HashMap;
import java.util.Map;

public class Student {
    private String jmeno;
    private String prijmeni;
    private Map<String, Integer> znamky;

    // Privátní konstruktor
    private Student(Builder builder) {
        this.jmeno = builder.jmeno;
        this.prijmeni = builder.prijmeni;
        this.znamky = builder.znamky;
    }

    // Gettery
    public String getJmeno() {
        return jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public Map<String, Integer> getZnamky() {
        return znamky;
    }

    // Metoda pro přidání nebo aktualizaci známky
    public void pridejZnamku(String predmet, Integer znamka) {
        znamky.put(predmet, znamka);
    }

    public double vypoctiPrumerZnamek() {
        if (this.znamky.isEmpty()) {
            return 0; // Pokud neexistují žádné známky, průměr je 0
        }
        return this.znamky.values().stream().mapToInt(Integer::intValue).average().orElse(0.0);
    }


    // Statická vnitřní třída Builder
    public static class Builder {
        private String jmeno;
        private String prijmeni;
        private Map<String, Integer> znamky = new HashMap<>();

        public Builder withJmeno(String jmeno) {
            this.jmeno = jmeno;
            return this;
        }

        public Builder withPrijmeni(String prijmeni) {
            this.prijmeni = prijmeni;
            return this;
        }

        //Nepoužívá se
        public Builder withZnamka(String predmet, Integer znamka) {
            this.znamky.put(predmet, znamka);
            return this;
        }

        public Student build() {
            return new Student(this);
        }
    }
}

