public class Trida {
    private String nazevTridy;
    private Student[] studenti;
    private int pocetStudentu = 0;

    private Trida(Builder builder) {
        this.nazevTridy = builder.nazevTridy;
        this.studenti = new Student[30]; // Maximálně 30 studentů
        System.arraycopy(builder.studenti, 0, this.studenti, 0, builder.pocetStudentu);
        this.pocetStudentu = builder.pocetStudentu;
    }

    // Gettery
    public String getNazevTridy() {
        return nazevTridy;
    }

    public Student[] getStudenti() {
        return studenti;
    }

    public int getPocetStudentu() {
        return pocetStudentu;
    }

    public double vypoctiTridniPrumer() {
        double sumaPrumeru = 0;
        int pocet = 0;
        for (int i = 0; i < pocetStudentu; i++) {
            double prumerStudenta = studenti[i].vypoctiPrumerZnamek();
            if (prumerStudenta > 0) { // Zahrnujeme pouze studenty s alespoň jednou známkou
                sumaPrumeru += prumerStudenta;
                pocet++;
            }
        }
        return pocet > 0 ? sumaPrumeru / pocet : 0;
    }


    public static class Builder {
        private String nazevTridy;
        private Student[] studenti = new Student[30];
        private int pocetStudentu = 0;

        public Builder withNazevTridy(String nazevTridy) {
            this.nazevTridy = nazevTridy;
            return this;
        }

        public Builder addStudent(Student student) {
            if (pocetStudentu < 30) { // Přidáme studenta pouze pokud je místo
                studenti[pocetStudentu++] = student;
            } else {
                throw new IllegalStateException("Nelze přidat více studentů, třída je plná.");
            }
            return this;
        }

        public Trida build() {
            return new Trida(this);
        }
    }
}

