import java.util.Map;

public interface ZnamkyProvider<T> {
    void loadData(String source, Map<String,T> tridy) throws Exception;
}
