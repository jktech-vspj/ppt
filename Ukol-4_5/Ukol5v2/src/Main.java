import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        // Vytvoření instance StudentLoader a volání metody loadData na instanci studentLoader
        StudentProvider<Trida> studentLoader = new StudentLoader();
        Map<String, Trida> tridy = studentLoader.loadData("studenti.xml");

        // Načtení známek pomocí instance ZnamkyLoader
        ZnamkyProvider<Trida> znamkyLoader = new ZnamkyLoader();
        znamkyLoader.loadData("znamky.csv", tridy);

        // Výpis studentů a jejich známek pro kontrolu
        for (Map.Entry<String, Trida> entry : tridy.entrySet()) {
            System.out.println("Trida: " + entry.getKey());
            Trida trida = entry.getValue();
            for (Student student : trida.getStudenti()) {
                if (student != null) { // Ošetření null hodnot
                    System.out.print(student.getJmeno() + " " + student.getPrijmeni() + ": ");
                    // Výpis známek studenta
                    student.getZnamky().forEach((predmet, znamka) ->
                            System.out.print(predmet + ":" + znamka + " "));
                    System.out.println(" celkový průměr: "+student.vypoctiPrumerZnamek()+"\n"); // Nový řádek po výpisu známek jednoho studenta
                }
            }
            System.out.println("Průměr třídy " + trida.getNazevTridy() + " je " + trida.vypoctiTridniPrumer() + "\n");
        }

        String exportType = "xml";
        Exporter exporter = ExporterFactory.getExporter(exportType);

        try
        {
            exporter.exportData(tridy);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
