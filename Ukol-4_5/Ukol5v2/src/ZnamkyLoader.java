import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ZnamkyLoader implements ZnamkyProvider<Trida> {


    @Override
    public void loadData(String souborZnamek, Map<String, Trida> tridy) throws Exception {
        // Rozhodnutí na základě typu souboru
        if (souborZnamek.endsWith(".csv")) {
            loadDataFromCsv(souborZnamek, tridy);
        }
        if (souborZnamek.endsWith(".xml")) {
            loadDataFromXml(souborZnamek, tridy);
        }
    }

    private void loadDataFromCsv(String souborZnamek, Map<String, Trida> tridy) {
        try (BufferedReader br = new BufferedReader(new FileReader(souborZnamek))) {
            String line;
            Trida aktualniTrida = null;
            int studentIndex = 0;
            while ((line = br.readLine()) != null) {
                // Kontrola, jestli řádek obsahuje název třídy
                if (tridy.containsKey(line)) {
                    aktualniTrida = tridy.get(line);
                    studentIndex = 0; // Resetování indexu studenta při změně třídy
                } else if (aktualniTrida != null && studentIndex < aktualniTrida.getPocetStudentu()) {
                    String[] znamky = line.split(";");
                    // Získání aktuálního studenta
                    Student student = aktualniTrida.getStudenti()[studentIndex];
                    if (znamky.length >= 5) {
                        // Přidání nebo aktualizace známek pro studenta
                        student.pridejZnamku("MAT", Integer.parseInt(znamky[0]));
                        student.pridejZnamku("FYZ", Integer.parseInt(znamky[1]));
                        student.pridejZnamku("IT", Integer.parseInt(znamky[2]));
                        student.pridejZnamku("TV", Integer.parseInt(znamky[3]));
                        student.pridejZnamku("CJ", Integer.parseInt(znamky[4]));
                    }
                    studentIndex++; // Posun na dalšího studenta pro příští iteraci
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDataFromXml(String soubor, Map<String, Trida> tridy) throws Exception {
        File xmlFile = new File(soubor);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();

        NodeList tridaList = doc.getElementsByTagName("trida");
        for (int i = 0; i < tridaList.getLength(); i++) {
            Node tridaNode = tridaList.item(i);
            if (tridaNode.getNodeType() == Node.ELEMENT_NODE) {
                Element tridaElement = (Element) tridaNode;
                String tridaNazev = tridaElement.getAttribute("nazev");
                Trida trida = tridy.get(tridaNazev);

                NodeList hodnoceniList = tridaElement.getElementsByTagName("hodnoceni");
                for (int j = 0; j < hodnoceniList.getLength(); j++) {
                    Node hodnoceniNode = hodnoceniList.item(j);
                    if (hodnoceniNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element hodnoceniElement = (Element) hodnoceniNode;
                        NodeList hodnotaList = hodnoceniElement.getElementsByTagName("hodnota");

                        if (j < trida.getPocetStudentu()) {
                            Student student = trida.getStudenti()[j];
                            for (int k = 0; k < hodnotaList.getLength(); k++) {
                                Node hodnotaNode = hodnotaList.item(k);
                                String hodnota = hodnotaNode.getTextContent();
                                String predmet = ""; // Zde je potřeba rozhodnout, jak přiřadit hodnoty k předmětům
                                switch (k) {
                                    case 0: predmet = "MAT"; break;
                                    case 1: predmet = "FYZ"; break;
                                    case 2: predmet = "IT"; break;
                                    case 3: predmet = "TV"; break;
                                    case 4: predmet = "CJ"; break;
                                    default: break; // V případě, že je více hodnot, než předmětů
                                }
                                student.pridejZnamku(predmet, Integer.parseInt(hodnota));
                            }
                        }
                    }
                }
            }
        }
    }
}
