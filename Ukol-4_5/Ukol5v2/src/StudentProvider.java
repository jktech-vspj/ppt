import java.util.Map;

public interface StudentProvider<T> {
    Map<String, T> loadData(String source) throws Exception;
}
