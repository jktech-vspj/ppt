import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.File;

public class StudentLoader implements StudentProvider<Trida> {


    @Override
    public Map<String, Trida> loadData(String source) throws Exception {
        // Rozhodneme na základě přípony souboru, zda se jedná o XML nebo CSV
        if (source.endsWith(".xml")) {
            return loadDataFromXml(source);
        }
        else if (source.endsWith(".csv")){
            // Implementace pro načítání z CSV by zůstala stejná jako v původním dotazu
            return loadDataFromCsv(source);
        }
        else
        {
            throw new Exception("Not implemented");
        }
    }
    private Map<String, Trida> loadDataFromCsv(String soubor) throws Exception {
        Map<String, Trida> tridy = new HashMap<>();
        Map<String, Trida.Builder> tridaBuilders = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(soubor))) {
            String line;
            Trida.Builder aktTridaBuilder = null;
            while ((line = br.readLine()) != null) {
                // Ignorujeme prázdné řádky
                if (line.trim().isEmpty()) continue;

                // Pokud je to název třídy
                if (line.matches("^\\d+[A-Z]$")) {
                    aktTridaBuilder = new Trida.Builder().withNazevTridy(line.trim());
                    tridaBuilders.put(line.trim(), aktTridaBuilder);
                } else {
                    // Načítání jména a příjmení studenta
                    String[] parts = line.split(";");
                    if (parts.length < 2) continue; // Zkontrolujte formát

                    Student student = new Student.Builder()
                            .withJmeno(parts[0])
                            .withPrijmeni(parts[1])
                            .build();

                    if (aktTridaBuilder != null) {
                        aktTridaBuilder.addStudent(student);
                    }
                }
            }

            // Převod Builderů na skutečné instance Trida
            for (Map.Entry<String, Trida.Builder> entry : tridaBuilders.entrySet()) {
                tridy.put(entry.getKey(), entry.getValue().build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tridy;
    }

    private  Map<String, Trida> loadDataFromXml(String soubor) throws Exception {
        Map<String, Trida.Builder> tridaBuilders = new HashMap<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(soubor));
        document.getDocumentElement().normalize();

        NodeList tridaList = document.getElementsByTagName("trida");
        for (int i = 0; i < tridaList.getLength(); i++) {
            Node tridaNode = tridaList.item(i);
            if (tridaNode.getNodeType() == Node.ELEMENT_NODE) {
                Element tridaElement = (Element) tridaNode;
                String tridaNazev = tridaElement.getAttribute("nazev");
                Trida.Builder tridaBuilder = new Trida.Builder().withNazevTridy(tridaNazev);

                NodeList osobaList = tridaElement.getElementsByTagName("osoba");
                for (int j = 0; j < osobaList.getLength(); j++) {
                    Node osobaNode = osobaList.item(j);
                    if (osobaNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element osobaElement = (Element) osobaNode;
                        String jmeno = osobaElement.getElementsByTagName("jmeno").item(0).getTextContent();
                        String prijmeni = osobaElement.getElementsByTagName("prijmeni").item(0).getTextContent();

                        Student student = new Student.Builder()
                                .withJmeno(jmeno)
                                .withPrijmeni(prijmeni)
                                .build();
                        tridaBuilder.addStudent(student);
                    }
                }
                tridaBuilders.put(tridaNazev, tridaBuilder);
            }
        }

        Map<String, Trida> tridy = new HashMap<>();
        // Převod Builderů na skutečné instance Trida
        for (Map.Entry<String, Trida.Builder> entry : tridaBuilders.entrySet()) {
            tridy.put(entry.getKey(), entry.getValue().build());
        }

        return tridy;
    }
}
