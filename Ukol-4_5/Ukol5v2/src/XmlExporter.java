import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.util.Map;

public class XmlExporter implements Exporter {
    @Override
    public void exportData(Map<String, Trida> tridy) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        for (Map.Entry<String, Trida> entry : tridy.entrySet()) {
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("skoly");
            doc.appendChild(rootElement);

            String tridaNazev = entry.getKey();
            Trida trida = entry.getValue();
            Element tridaElement = doc.createElement("trida");
            tridaElement.setAttribute("nazev", tridaNazev);
            rootElement.appendChild(tridaElement);

            for (Student student : trida.getStudenti()) {
                if (student != null) {
                    Element studentElement = doc.createElement("student");

                    Element jmenoElement = doc.createElement("jmeno");
                    jmenoElement.appendChild(doc.createTextNode(student.getJmeno() + " " + student.getPrijmeni()));
                    studentElement.appendChild(jmenoElement);

                    Element prumerElement = doc.createElement("prumer");
                    prumerElement.appendChild(doc.createTextNode(String.valueOf(student.vypoctiPrumerZnamek())));
                    studentElement.appendChild(prumerElement);

                    tridaElement.appendChild(studentElement);
                }
            }

            Element prumerTridyElement = doc.createElement("prumerTridy");
            prumerTridyElement.appendChild(doc.createTextNode(String.valueOf(trida.vypoctiTridniPrumer())));
            tridaElement.appendChild(prumerTridyElement);

            // Název souboru dle názvu třídy
            String fileName = tridaNazev + ".xml";
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(fileName));

            transformer.transform(source, result);
        }
    }
}
