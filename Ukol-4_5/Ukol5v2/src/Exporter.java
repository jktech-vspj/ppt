import java.util.Map;

public interface Exporter {
    void exportData(Map<String, Trida> tridy) throws Exception;
}
