import java.util.Map;

class QuadraticEquationResult {
    private double a, b, c;
    private Map<Double, Double> results;

    public QuadraticEquationResult(double a, double b, double c, Map<Double, Double> results) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.results = results;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a).append(';').append(b).append(';').append(c);
        for (Map.Entry<Double, Double> entry : results.entrySet()) {
            sb.append(';').append(entry.getKey()).append(':').append(entry.getValue());
        }
        return sb.toString();
    }
}