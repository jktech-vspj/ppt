import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

class CSVDataProvider extends DataProvider {
    public CSVDataProvider(DataLoader loader, DataWriter writer) {
        super(loader, writer);
    }

    @Override
    public void execute() {
        try {
            List<QuadraticEquation> equations = loader.load();
            List<QuadraticEquationResult> results = equations.stream()
                    .map(QuadraticEquation::computeResults)
                    .collect(Collectors.toList());
            writer.write(results);
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            // další zacházení s chybami
        }
    }
}