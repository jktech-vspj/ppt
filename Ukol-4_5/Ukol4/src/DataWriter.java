import java.io.IOException;
import java.util.List;

abstract class DataWriter {
    protected String filename;

    public DataWriter(String filename) {
        this.filename = filename;
    }

    public abstract void write(List<QuadraticEquationResult> results) throws IOException;
}