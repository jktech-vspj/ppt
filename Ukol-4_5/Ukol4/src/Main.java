public class Main {
    public static void main(String[] args) {
        String inputFilename = "data.csv"; // Název souboru se vstupními daty
        if (args.length > 0) {
            inputFilename = args[0];
        }

        DataProvider provider = ProviderBuilder.createCSVProvider(inputFilename);
        provider.execute();
    }
}
