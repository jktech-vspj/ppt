import java.io.IOException;
import java.util.List;

abstract class DataLoader {
    protected String filename;

    public DataLoader(String filename) {
        this.filename = filename;
    }

    public abstract List<QuadraticEquation> load() throws IOException;
}
