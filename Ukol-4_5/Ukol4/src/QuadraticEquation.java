import java.util.HashMap;
import java.util.Map;

class QuadraticEquation {
    private double a, b, c;
    private double[] xs;

    public QuadraticEquation(double a, double b, double c, double[] xs) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.xs = xs;
    }

    public QuadraticEquationResult computeResults() {
        Map<Double, Double> results = new HashMap<>();
        for (double x : xs) {
            double y = a * x * x + b * x + c;
            results.put(x, y);
        }
        return new QuadraticEquationResult(a, b, c, results);
    }
}
