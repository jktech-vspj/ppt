import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
class CSVDataWriter extends DataWriter {
    public CSVDataWriter(String filename) {
        super(filename);
    }

    @Override
    public void write(List<QuadraticEquationResult> results) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            for (QuadraticEquationResult result : results) {
                bw.write(result.toString());
                bw.newLine();
            }
        }
    }
}