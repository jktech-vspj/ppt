class ProviderBuilder {
    public static DataProvider createCSVProvider(String filename) {
        DataLoader loader = new CSVDataLoader(filename);
        DataWriter writer = new CSVDataWriter("output_" + filename);
        return new CSVDataProvider(loader, writer);
    }
}
