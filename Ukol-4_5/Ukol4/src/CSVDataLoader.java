import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CSVDataLoader extends DataLoader {
    public CSVDataLoader(String filename) {
        super(filename);
    }

    @Override
    public List<QuadraticEquation> load() throws IOException {
        List<QuadraticEquation> equations = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(";");
                double a = Double.parseDouble(parts[0]);
                double b = Double.parseDouble(parts[1]);
                double c = Double.parseDouble(parts[2]);
                double[] xs = Arrays.stream(parts).skip(3).mapToDouble(Double::parseDouble).toArray();
                equations.add(new QuadraticEquation(a, b, c, xs));
            }
        }
        return equations;
    }
}
