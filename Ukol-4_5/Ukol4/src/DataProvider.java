abstract class DataProvider {
    protected DataLoader loader;
    protected DataWriter writer;

    public DataProvider(DataLoader loader, DataWriter writer) {
        this.loader = loader;
        this.writer = writer;
    }

    public abstract void execute();
}
