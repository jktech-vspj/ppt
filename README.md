### Pokročilé programovací techniky
Vypracovaná cvičení do předmětu Pokročilé programovací techniky na VSPJ.

### Nástroje
- [Visual Studio](https://visualstudio.microsoft.com/)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
