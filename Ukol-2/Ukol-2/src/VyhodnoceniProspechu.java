public class VyhodnoceniProspechu {
    public static String prospech(int[] znamky)
    {
        double avg = prumer(znamky);
        if (avg < 1.5) return "Výborně";
        if (avg < 2.5) return "Chvalitebně";
        if (avg < 3.5) return "Dobře";
        if (avg < 4.5) return "Dostatečně";
        return "Nedostatečně";
    }

    private static double prumer(int[] znamky)
    {
        if (znamky.length == 0) throw new IllegalArgumentException("Pole nesmí být prázdný");
        int sum = 0;
        for (int mark : znamky)
        {
            sum += mark;
        }
        return (double) sum / znamky.length;
    }
}
