import static org.junit.jupiter.api.Assertions.*;

class ZpracovaniMyTest
{

    @org.junit.jupiter.api.Test
    public void testCompInt() {
        assertEquals(-1, ZpracovaniMy.comp(5, 10));
        assertEquals(1, ZpracovaniMy.comp(10, 5));
        assertEquals(0, ZpracovaniMy.comp(10, 10));
    }

@org.junit.jupiter.api.Test
    public void testCompString() {
        assertEquals(-1, ZpracovaniMy.comp("apple", "banana"));
        assertEquals(1, ZpracovaniMy.comp("banana", "apple"));
        assertEquals(0, ZpracovaniMy.comp("apple", "apple"));
        assertEquals(1, ZpracovaniMy.comp("test", "Test"));
    }
}