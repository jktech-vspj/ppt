public class ZpracovaniMy
{
    public static int comp(int a, int b)
    {
        if (a < b) return -1;
        if (a > b) return 1;
        return 0;
    }

    public static int comp(String a, String b)
    {
        int out = a.compareTo(b);
        if (out < 0) return -1;
        if (out > 0) return 1;
        return 0;
    }
}
