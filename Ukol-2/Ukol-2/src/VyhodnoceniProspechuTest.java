import static org.junit.jupiter.api.Assertions.*;

class VyhodnoceniProspechuTest {

    @org.junit.jupiter.api.Test
    public void testProspech() {
        assertEquals("Výborně", VyhodnoceniProspechu.prospech(new int[] {1, 2, 1, 1}));
        assertEquals("Chvalitebně", VyhodnoceniProspechu.prospech(new int[] {2, 3, 2, 2}));
        assertEquals("Dobře", VyhodnoceniProspechu.prospech(new int[] {3, 3, 4}));
        assertEquals("Dostatečně", VyhodnoceniProspechu.prospech(new int[] {3, 4, 4, 5}));
        assertEquals("Nedostatečně", VyhodnoceniProspechu.prospech(new int[] {4, 5, 5, 5}));
    }

    @org.junit.jupiter.api.Test
    public void testProspechWithBoundaryValues() {
        assertEquals("Výborně", VyhodnoceniProspechu.prospech(new int[] {1, 1, 2}));
        assertEquals("Chvalitebně", VyhodnoceniProspechu.prospech(new int[] {2, 2, 2, 3}));
        assertEquals("Dobře", VyhodnoceniProspechu.prospech(new int[] {3, 3, 3, 4}));
        assertEquals("Dostatečně", VyhodnoceniProspechu.prospech(new int[] {3, 4, 4, 4}));
        assertEquals("Nedostatečně", VyhodnoceniProspechu.prospech(new int[] {4, 5, 5}));
    }

    @org.junit.jupiter.api.Test
    public void testEmptyArray() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            VyhodnoceniProspechu.prospech(new int[] {});
        });
        assertEquals("Pole nesmí být prázdný", exception.getMessage());
    }
}
