import java.security.InvalidParameterException;

public class Nadrz {

    private double kapacita;

    private double stav;

    Nadrz(double kapacita) {
        set_kapacita(kapacita);//this.kapacita = kapacita;
        stav = 0.0;
    }

    private void set_kapacita(double kapacita) {
        if (kapacita > 0.0) {
            this.kapacita = kapacita;
            return;
        }

        throw new InvalidParameterException("Neplatna kapacita nadrze");
    }

    public double get_kapacita()
    {
        return kapacita;
    }

    public void add(double kolik) {
        if (kolik < 0.0) {
            throw new InvalidParameterException("Neplatna hodnota pro odebrani");
        }

        final double stav_new = stav + kolik;
        if (stav_new > kapacita) {
            throw new InvalidParameterException("Neplatna hodnota pro odebrani");
        }

        stav = stav_new;
    }

    public void remove(double kolik) {
        if (kolik < 0.0) {
            throw new InvalidParameterException("Neplatna hodnota pro odebrani");
        }

        final double stav_new = stav - kolik;
        if (stav_new < 0.0) {
            throw new InvalidParameterException("V nadrzi neni dostatek obsahu pro odebrani");
        }

        stav = stav_new;
    }
}
