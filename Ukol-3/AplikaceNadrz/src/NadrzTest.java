import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

class NadrzTest {
    @org.junit.jupiter.api.Test
    void create() {
        assertDoesNotThrow(() -> {
            Nadrz NZ = new Nadrz(10);
        });

        assertThrows(InvalidParameterException.class, () -> {
            Nadrz NZ = new Nadrz(-10);
        });

        assertThrows(InvalidParameterException.class, () -> {
            Nadrz NZ = new Nadrz(0);
        });
    }

    @org.junit.jupiter.api.Test
    void test() {
        Nadrz nz = new Nadrz(100);

        assertEquals(100, nz.get_kapacita());
        assertNotEquals(10, nz.get_kapacita());

        assertDoesNotThrow(() -> {
            nz.add(10);
        });

        //Pridani nuloveho objemu
        assertDoesNotThrow(() -> {
            nz.add(0);
        });

        //Pridani zaporneho objemu
        assertThrows(InvalidParameterException.class, () -> {
            nz.add(-10);
        });

        //Pridani objemu mimo kapacitu
        assertThrows(InvalidParameterException.class, () -> {
            nz.add(100);
        });

        assertDoesNotThrow(() -> {
            nz.remove(10);
        });

        // Odebrani nuloveho objemu
        assertDoesNotThrow(() -> {
            nz.remove(0);
        });

        // Odebrani vetsiho objemu nez je objem v nadrzi
        assertThrows(InvalidParameterException.class, () -> {
            nz.remove(10);
        });

        // Odebrani zaporneho objemu
        assertThrows(InvalidParameterException.class, () -> {
            nz.remove(-10);
        });
    }
}