package javaapplication_ppt_histogram;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class VypocetTest {

    @org.junit.jupiter.api.Test
    void test() {
        // Vsechna pismena abecedy
        Vypocet vypocet = new Vypocet();
        String text1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Map<Character, Integer> expectedHistogram1 = new HashMap<>();

        for (char c = 'A'; c <= 'Z'; c++) {
            expectedHistogram1.put(c, 1);
        }

        vypocet.pocitej(text1);
        Map<Character, Integer> actualHistogram1 = vypocet.getHistogram();

        // Assert
        assertEquals(expectedHistogram1, actualHistogram1);

        // Prazdny string
        String text2 = "";
        Map<Character, Integer> expectedHistogram2 = new HashMap<>();

        for (char c = 'A'; c <= 'Z'; c++) {
            expectedHistogram2.put(c, 0);
        }

        // Act
        vypocet.pocitej(text2);
        Map<Character, Integer> actualHistogram2 = vypocet.getHistogram();

        // Assert
        assertEquals(expectedHistogram2, actualHistogram2);

        String text3 = "POKROCILEPROGRAMOVACITECHNIKY";
        Map<Character, Integer> expectedHistogram3 = new HashMap<>();

        for (char c = 'A'; c <= 'Z'; c++) {
            expectedHistogram3.put(c, 0);
        }

        expectedHistogram3.put('A', 2);
        expectedHistogram3.put('C', 3);
        expectedHistogram3.put('E', 2);
        expectedHistogram3.put('G', 1);
        expectedHistogram3.put('H', 1);
        expectedHistogram3.put('I', 3);
        expectedHistogram3.put('K', 2);
        expectedHistogram3.put('L', 1);
        expectedHistogram3.put('M', 1);
        expectedHistogram3.put('N', 1);
        expectedHistogram3.put('O', 4);
        expectedHistogram3.put('P', 2);
        expectedHistogram3.put('R', 3);
        expectedHistogram3.put('T', 1);
        expectedHistogram3.put('V', 1);
        expectedHistogram3.put('Y', 1);

        // Act
        vypocet.pocitej(text3);
        Map<Character, Integer> actualHistogram = vypocet.getHistogram();

        // Assert
        assertEquals(expectedHistogram3, actualHistogram);
    }
}