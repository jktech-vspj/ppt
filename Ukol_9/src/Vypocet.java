/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_ppt_histogram;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author musil19
 */
public class Vypocet {
    private Map<Character, Integer> histogram;
    public Vypocet() {
        histogram = new HashMap<>();
    }

    public void pocitej(String text) {
        try {
            // Inicializace histogramu
            for (char c = 'A'; c <= 'Z'; c++) {
                histogram.put(c, 0);
            }

            // Vytvoření histogramu
            for (char z : text.toCharArray()) {
                if (Character.isLetter(z) && Character.isUpperCase(z)) {
                    histogram.put(z, histogram.get(z) + 1);
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Map<Character, Integer> getHistogram() {
        return histogram;
    }
}
