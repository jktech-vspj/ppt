import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CastkaDokladuTest {

    @Test
    public void testInicializaceCastkyDokladu() {
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        assertNotNull(castkaDokladu);
    }

    @Test
    public void testZapornaHodnotaCenyBezDPH() {
        assertThrows(IllegalArgumentException.class, () -> {
            new CastkaDokladu(-100.0, 121.0, "DPH 21%", 21);
        });
    }

    @Test
    public void testZapornaHodnotaCenySDPH() {
        assertThrows(IllegalArgumentException.class, () -> {
            new CastkaDokladu(100.0, -121.0, "DPH 21%", 21);
        });
    }

    @Test
    public void testZapornaHodnotaHodnotySazby() {
        assertThrows(IllegalArgumentException.class, () -> {
            new CastkaDokladu(100.0, 121.0, "DPH 21%", -21);
        });
    }
}