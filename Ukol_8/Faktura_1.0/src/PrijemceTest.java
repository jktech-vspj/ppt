import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PrijemceTest {

    @Test
    public void testInicializacePrijemce() {
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");

        assertNotNull(prijemce);
    }
}