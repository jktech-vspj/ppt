import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class FakturaTest {

    @Test
    public void testInicializaceFaktury() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000);
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertNotNull(faktura);
    }

    @Test
    public void testNespravnyDatumSplatnosti() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() - 86400000);
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        assertThrows(IllegalArgumentException.class, () -> {
            new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);
        });
    }
}