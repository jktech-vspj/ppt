import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class FakturaTest {

    @Test
    public void testInicializaceFaktury() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000); // Přidání jednoho dni
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertNotNull(faktura);
    }

    @Test
    public void testNespravnyDatumSplatnosti() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() - 86400000); // Odebrání jednoho dne
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        assertThrows(IllegalArgumentException.class, () -> {
            new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);
        });
    }

    @Test
    public void testGetCenaBezDPH() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000); // Přidání jednoho dni
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertEquals(100.0, faktura.getCenaBezDPH());
    }

    @Test
    public void testGetCenaSDPH() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000); // Přidání jednoho dni
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertEquals(121.0, faktura.getCenaSDPH());
    }

    @Test
    public void testGetSazba() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000); // Přidání jednoho dni
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertEquals("DPH 21%", faktura.getSazba());
    }

    @Test
    public void testGetHodnotaSazby() {
        Date datumVystaveni = new Date();
        Date datumSplatnosti = new Date(datumVystaveni.getTime() + 86400000); // Přidání jednoho dni
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");
        CastkaDokladu castkaDokladu = new CastkaDokladu(100.0, 121.0, "DPH 21%", 21);

        Faktura faktura = new Faktura("123", datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);

        assertEquals(21, faktura.getHodnotaSazby());
    }
}