import java.util.Date;

public class Faktura {
    private final String cisloDokladu;
    private final Date datumVystaveni;
    private final Date datumSplatnosti;
    private final Prijemce prijemce;
    private final CastkaDokladu castkaDokladu;

    public Faktura(String cisloDokladu, Date datumVystaveni, Date datumSplatnosti, Prijemce prijemce, CastkaDokladu castkaDokladu) {
        this.cisloDokladu = cisloDokladu;
        this.datumVystaveni = datumVystaveni;
        // Ověření, že datum splatnosti je později než datum vystavení
        if (datumSplatnosti.before(datumVystaveni)) {
            throw new IllegalArgumentException("Datum splatnosti musí být později než datum vystavení.");
        }
        this.datumSplatnosti = datumSplatnosti;
        this.prijemce = prijemce;
        this.castkaDokladu = castkaDokladu;
    }

    public Prijemce getPrijemce() {
        return prijemce;
    }

    public CastkaDokladu getCastkaDokladu() {
        return castkaDokladu;
    }

    public double getCenaBezDPH() {
        return castkaDokladu.getCenaBezDPH();
    }

    public double getCenaSDPH() {
        return castkaDokladu.getCenaSDPH();
    }

    public String getSazba() {
        return castkaDokladu.getSazba();
    }

    public int getHodnotaSazby() {
        return castkaDokladu.getHodnotaSazby();
    }

    @Override
    public String toString() {
        return "Faktura číslo: " + cisloDokladu + "\n" +
                "Datum vystavení: " + datumVystaveni + "\n" +
                "Datum splatnosti: " + datumSplatnosti + "\n" +
                "Příjemce: " + prijemce.getJmeno() + " " + prijemce.getPrijmeni() +  "\n" +
                "Částky dokladu: " + castkaDokladu.getCenaSDPH() + ",- s DPH";
    }
}
