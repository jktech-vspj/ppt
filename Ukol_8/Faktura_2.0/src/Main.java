import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Prijemce prijemce = new Prijemce("Jan", "Novák", "Ulice 123", "Město", "12345", "123456789", "jan@novak.cz");

        Faktura faktura = new FakturaBuilder("123", new Date(), new Date(), prijemce, 100.0, ZauctujFakturu.DPH_SAZBA_21)
                .build();

        System.out.println(faktura);
    }
}
