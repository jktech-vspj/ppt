public class CastkaDokladu {
    private final double cenaBezDPH;
    private final double cenaSDPH;
    private final String sazba;
    private final int hodnotaSazby;

    public CastkaDokladu(double cenaBezDPH, double cenaSDPH, String sazba, int hodnotaSazby) {
        if (cenaBezDPH < 0 || cenaSDPH < 0 || hodnotaSazby < 0) {
            throw new IllegalArgumentException("Hodnoty ceny a hodnoty sazby nesmějí být záporné.");
        }
        this.cenaBezDPH = cenaBezDPH;
        this.cenaSDPH = cenaSDPH;
        this.sazba = sazba;
        this.hodnotaSazby = hodnotaSazby;
    }

    public double getCenaBezDPH() {
        return cenaBezDPH;
    }

    public double getCenaSDPH() {
        return cenaSDPH;
    }

    public String getSazba() {
        return sazba;
    }

    public int getHodnotaSazby() {
        return hodnotaSazby;
    }
}
