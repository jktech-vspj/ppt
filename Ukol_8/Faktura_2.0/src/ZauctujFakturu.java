public class ZauctujFakturu {
    public static final int DPH_SAZBA_21 = 21;
    public static final int DPH_SAZBA_15 = 15;

    public static double spocitejCenuSDPH(double cenaBezDPH, double sazba) {
        return cenaBezDPH * (1 + sazba / 100);
    }

    public static double spocitejDPH(double cenaBezDPH, double sazba) {
        return cenaBezDPH * sazba / 100;
    }
}