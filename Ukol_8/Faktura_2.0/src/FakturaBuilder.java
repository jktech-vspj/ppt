import java.util.Date;

public class FakturaBuilder {
    private final String cisloDokladu;
    private final Date datumVystaveni;
    private final Date datumSplatnosti;
    private final Prijemce prijemce;
    private CastkaDokladu castkaDokladu;

    public FakturaBuilder(String cisloDokladu, Date datumVystaveni, Date datumSplatnosti, Prijemce prijemce, double cenaBezDPH, int sazba) {
        this.cisloDokladu = cisloDokladu;
        this.datumVystaveni = datumVystaveni;
        this.datumSplatnosti = datumSplatnosti;
        this.prijemce = prijemce;
        double cenaSDPH = ZauctujFakturu.spocitejCenuSDPH(cenaBezDPH, sazba);
        double dph = ZauctujFakturu.spocitejDPH(cenaBezDPH, sazba);
        this.castkaDokladu = new CastkaDokladu(cenaBezDPH, cenaSDPH, "DPH " + sazba + "%", sazba);
    }

    public FakturaBuilder setCenaBezDPH(double cenaBezDPH) {
        double cenaSDPH = ZauctujFakturu.spocitejCenuSDPH(cenaBezDPH, castkaDokladu.getHodnotaSazby());
        double dph = ZauctujFakturu.spocitejDPH(cenaBezDPH, castkaDokladu.getHodnotaSazby());
        this.castkaDokladu = new CastkaDokladu(cenaBezDPH, cenaSDPH, castkaDokladu.getSazba(), castkaDokladu.getHodnotaSazby());
        return this;
    }

    public FakturaBuilder setSazba(int sazba) {
        double cenaSDPH = ZauctujFakturu.spocitejCenuSDPH(castkaDokladu.getCenaBezDPH(), sazba);
        double dph = ZauctujFakturu.spocitejDPH(castkaDokladu.getCenaBezDPH(), sazba);
        this.castkaDokladu = new CastkaDokladu(castkaDokladu.getCenaBezDPH(), cenaSDPH, "DPH " + sazba + "%", sazba);
        return this;
    }

    public Faktura build() {
        return new Faktura(cisloDokladu, datumVystaveni, datumSplatnosti, prijemce, castkaDokladu);
    }
}